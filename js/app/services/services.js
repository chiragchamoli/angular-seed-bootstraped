(function() {
    'use strict';

    /* Services */

    var appServices = angular.module('sampleApp.rootService', []);
    // Demonstrate how to register services
    // In this case it is a simple value service.

    appServices.value('version', '0.1');
})();
