(function() {
    'use strict';

    /* Directives */
    var appDirective = angular.module('sampleApp.rootDirective', []);

    appDirective.directive('appVersion', ['version',
        function(version) {
            return function(scope, elm, attrs) {
                elm.text(version);
            };
        }
    ]);

})();
