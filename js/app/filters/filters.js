(function() {
    'use strict';

    /* Filters */
    var appFilters = angular.module('sampleApp.rootFilter', []);


    appFilters.filter('interpolate', ['version',
        function(version) {
            return function(text) {
                return String(text).replace(/\%VERSION\%/mg, version);
            }
        }
    ]);
})();
