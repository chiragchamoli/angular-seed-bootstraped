(function() {
    'use strict';
    // Declare app level module which depends on filters, and services
    var bootstrapApp = angular.module('sampleApp', [
        'sampleApp.rootFilter',
        'sampleApp.rootService',
        'sampleApp.rootDirective',
        'sampleApp.rootController'
    ]);

    bootstrapApp.config(['$routeProvider',
        function($routeProvider) {

            $routeProvider.when('/', {
                templateUrl: 'partials/home.html',
                controller: 'home'
            });
            $routeProvider.when('/form', {
                templateUrl: 'partials/partial1.html',
                controller: 'MyCtrl1'
            });
            $routeProvider.when('/grid', {
                templateUrl: 'partials/partial2.html',
                controller: 'MyCtrl2'
            });
            $routeProvider.otherwise({
                redirectTo: '/form'
            });
        }
    ]);
})();
